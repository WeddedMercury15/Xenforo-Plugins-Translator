package gui

import (
	"Xenforo-Translator/pkg/translator"
	"Xenforo-Translator/pkg/xmlhandler"

	"fyne.io/fyne/v2"
	"fyne.io/fyne/v2/app"
	"fyne.io/fyne/v2/container"
	"fyne.io/fyne/v2/dialog"
	"fyne.io/fyne/v2/widget"
)

func Run() {
	a := app.New()
	w := a.NewWindow("Xenforo Language Translator")

	filePath := widget.NewLabel("")

	openFileButton := widget.NewButton("Open File", func() {
		dialog.ShowFileOpen(func(reader fyne.URIReadCloser, err error) {
			if err == nil && reader != nil {
				filePath.SetText(reader.URI().Path())
			}
		}, w)
	})

	translateButton := widget.NewButton("Translate", func() {
		translateButton := widget.NewButton("Translate", nil)
		progressBar := widget.NewProgressBar()

		go func() {
			langData, err := xmlhandler.ParseXML(filePath.Text)
			if err != nil {
				dialog.ShowError(err, w)
				return
			}

			err = translator.TranslatePhrases(langData, "en", "zh-cn", func(done float64) {
				progressBar.SetValue(done)
			})
			if err != nil {
				dialog.ShowError(err, w)
				return
			}

			err = xmlhandler.WriteXML(langData, filePath.Text)
			if err != nil {
				dialog.ShowError(err, w)
			} else {
				dialog.ShowInformation("Success", "Translation completed!", w)
			}
			progressBar.SetValue(0)
		}()
	})

	w.SetContent(container.NewVBox(
		filePath,
		openFileButton,
		translateButton,
	))

	w.ShowAndRun()
}
