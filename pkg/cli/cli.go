package cli

import (
	"Xenforo-Translator/pkg/translator"
	"Xenforo-Translator/pkg/xmlhandler"
	"fmt"
)

func Run(filePath string) {
	// 命令行逻辑
	langData, err := xmlhandler.ParseXML(filePath)
	if err != nil {
		fmt.Println("解析XML时发生错误：", err)
		return
	}

	err = translator.TranslatePhrases(langData, "en", "zh-cn")
	if err != nil {
		fmt.Println("翻译时发生错误：", err)
		return
	}

	err = xmlhandler.WriteXML(langData, filePath)
	if err != nil {
		fmt.Println("写入XML时发生错误：", err)
		return
	}

	fmt.Println("翻译完成，结果已保存。")
}
