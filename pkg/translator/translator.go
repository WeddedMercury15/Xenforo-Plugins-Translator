package translator

import (
	"Xenforo-Translator/pkg/xmlhandler"
	"strings"

	gt "github.com/bregydoc/gtranslate"
	"golang.org/x/text/language"
)

func TranslatePhrases(langData *xmlhandler.Language, sourceLang, targetLang string) error {
	sourceLangTag := language.Make(sourceLang)
	targetLangTag := language.Make(targetLang)

	for i, elem := range langData.Phrases {
		if elem.Text != "" && strings.TrimSpace(elem.Text) != "" {
			cdataContent := strings.TrimSuffix(strings.TrimPrefix(elem.Text, "<![CDATA["), "]]>")
			if cdataContent != "" {
				translatedText, err := gt.Translate(cdataContent, sourceLangTag, targetLangTag)
				if err != nil {
					return err
				}
				langData.Phrases[i].Text = "<![CDATA[" + translatedText + "]]>"
			}
		}
	}
	return nil
}
