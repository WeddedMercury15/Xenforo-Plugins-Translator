package xmlhandler

import (
	"bufio"
	"encoding/xml"
	"io/ioutil"
	"os"
	"strings"

	"golang.org/x/net/html/charset"
)

type Phrase struct {
	XMLName       xml.Name `xml:"phrase"`
	Title         string   `xml:"title,attr"`
	AddonID       string   `xml:"addon_id,attr"`
	VersionID     string   `xml:"version_id,attr"`
	VersionString string   `xml:"version_string,attr"`
	Text          string   `xml:",innerxml"`
}

type Language struct {
	XMLName            xml.Name `xml:"language"`
	Title              string   `xml:"title,attr"`
	DateFormat         string   `xml:"date_format,attr"`
	TimeFormat         string   `xml:"time_format,attr"`
	CurrencyFormat     string   `xml:"currency_format,attr"`
	WeekStart          string   `xml:"week_start,attr"`
	DecimalPoint       string   `xml:"decimal_point,attr"`
	ThousandsSeparator string   `xml:"thousands_separator,attr"`
	LabelSeparator     string   `xml:"label_separator,attr"`
	CommaSeparator     string   `xml:"comma_separator,attr"`
	Ellipsis           string   `xml:"ellipsis,attr"`
	ParenthesisOpen    string   `xml:"parenthesis_open,attr"`
	ParenthesisClose   string   `xml:"parenthesis_close,attr"`
	LanguageCode       string   `xml:"language_code,attr"`
	TextDirection      string   `xml:"text_direction,attr"`
	UserSelectable     string   `xml:"user_selectable,attr"`
	AddonID            string   `xml:"addon_id,attr"`
	BaseVersionID      string   `xml:"base_version_id,attr"`
	ExportVersion      string   `xml:"export_version,attr"`
	Phrases            []Phrase `xml:"phrase"`
}

func ParseXML(filePath string) (*Language, error) {
	content, err := ioutil.ReadFile(filePath)
	if err != nil {
		return nil, err
	}

	var languageData Language
	decoder := xml.NewDecoder(strings.NewReader(string(content)))
	decoder.CharsetReader = charset.NewReaderLabel
	err = decoder.Decode(&languageData)
	if err != nil {
		return nil, err
	}

	return &languageData, nil
}

func WriteXML(langData *Language, filePath string) error {
	outputFilePath := strings.Replace(filePath, ".xml", "_translated.xml", 1)
	outputFile, err := os.Create(outputFilePath)
	if err != nil {
		return err
	}
	defer outputFile.Close()

	writer := bufio.NewWriter(outputFile)
	_, err = writer.WriteString(`<?xml version="1.0" encoding="utf-8"?>` + "\n")
	if err != nil {
		return err
	}

	encoder := xml.NewEncoder(writer)
	encoder.Indent("", "  ")
	err = encoder.Encode(langData)
	if err != nil {
		return err
	}
	writer.Flush()

	return nil
}
