package main

import (
	"Xenforo-Translator/pkg/cli"
	"fmt"
	"os"
	"runtime"
	"strings"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Println("使用方法: go run script.go <file_path>")
		return
	}
	filePath := os.Args[1]

	if hasDesktopEnvironment() {
		cli.Run(filePath)
	} else {
		cli.Run(filePath)
	}
}

// hasDesktopEnvironment 检查是否有桌面环境
func hasDesktopEnvironment() bool {
	switch runtime.GOOS {
	case "windows":
		// 检查 SESSIONNAME 环境变量
		if sessionName, ok := os.LookupEnv("SESSIONNAME"); ok {
			return !strings.EqualFold(sessionName, "Console") // Console 表示没有GUI
		}
	case "linux", "darwin":
		// 对于 UNIX 系统，检查 DISPLAY (X11) 或 WAYLAND_DISPLAY (Wayland) 环境变量
		if _, displaySet := os.LookupEnv("DISPLAY"); displaySet {
			return true
		}
		if _, waylandSet := os.LookupEnv("WAYLAND_DISPLAY"); waylandSet {
			return true
		}
	}
	return false // 默认情况下，假设没有桌面环境
}
