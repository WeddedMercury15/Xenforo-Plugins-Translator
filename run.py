import sys
from lxml import etree
from googletrans import Translator

def translate(file_path, source_lang, target_lang):
    # 创建一个Translator对象
    translator = Translator(service_urls=['translate.google.com'])

    # 解析XML文件
    parser = etree.XMLParser(strip_cdata=False)
    tree = etree.parse(file_path, parser)
    root = tree.getroot()

    # 遍历XML文件中的每个元素
    for elem in root.iter('phrase'):
        # 检查元素的文本是否为None
        if elem.text is not None and elem.text.strip('<![CDATA[]]>') != '':
            # 获取元素的文本（CDATA部分）
            text = elem.text.strip('<![CDATA[]]>')
            # 使用Googletrans来翻译文本
            translated_text = translator.translate(text, src=source_lang, dest=target_lang).text
            # 将元素的文本替换为翻译后的文本，并保留CDATA标签
            elem.text = '<![CDATA[' + translated_text + ']]>'
        else:
            # 如果元素没有文本，将其文本设置为<![CDATA[]]>
            elem.text = '<![CDATA[]]>'

    # 将翻译后的XML文件保存到新的文件中
    output_file_path = file_path.replace('.xml', '_translated.xml')

    # 在文件的第一行插入XML声明，并将转义后的字符替换回原来的字符
    with open(output_file_path, 'w', encoding='utf-8') as f:
        f.write('<?xml version="1.0" encoding="utf-8"?>\n')
        f.write(etree.tostring(root, encoding='unicode'))

    # 读取文件内容，将<和>替换为<和>，然后写回文件
    with open(output_file_path, 'r', encoding='utf-8') as f:
        content = f.read()
    with open(output_file_path, 'w', encoding='utf-8') as f:
        f.write(content.replace('<![CDATA[', '<![CDATA[').replace(']]>', ']]>'))

if __name__ == "__main__":
    # 从命令行参数获取文件名
    file_path = sys.argv[1]
    # 调用函数进行翻译
    translate(file_path, 'en', 'zh-cn')
